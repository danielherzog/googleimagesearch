//
//  ListViewController.m
//  GoogleImageSearch
//
//  Created by Daniel Herzog on 04.01.16.
//  Copyright (c) 2016 Daniel Herzog. All rights reserved.
//

#import "ListViewController.h"
#import "DetailViewController.h"

@interface ListViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UITextField *searchText;
@property (nonatomic, strong) UIButton *searchButton;
@property (nonatomic, strong) NSArray *imageResults;
@property (nonatomic, strong) NSString *cellIdentifier;
@property (nonatomic, strong) UIActivityIndicatorView *spinner;


@end

@implementation ListViewController

- (IBAction) fetch:(NSString *) inputText {
    
    NSString *url = @"https://fhtw-google-image-search.herokuapp.com/ajax/services/search/images/?q=%@";
    
    NSURL *urlWithQuery = [NSURL URLWithString:[NSString stringWithFormat:url, inputText]];
    NSURLRequest *req = [NSURLRequest requestWithURL:urlWithQuery];
    [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if(data.length > 0 && connectionError == nil){
            NSDictionary *res = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
            
            id responseData = [res objectForKey:@"responseData"];
            NSArray *results = [responseData objectForKey:@"results"];
            
            self.imageResults = results;
            [self.tableView reloadData];
            [self.spinner removeFromSuperview];
            
        }
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    
    self.searchButton = [[UIButton alloc] init];
    self.searchButton = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    self.searchButton.frame = CGRectMake(0, 80, 100, 20);
    [self.searchButton setTitle:@"Search" forState:UIControlStateNormal];
    [self.searchButton addTarget:self  action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.searchButton];
    
    self.searchText = [[UITextField alloc] init];
    self.searchText.frame = CGRectMake(100, 80, 250, 20);
    [self.view addSubview:self.searchText];
    
    //table view
    self.cellIdentifier = @"cell";
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    self.tableView.frame = CGRectMake(0, 120, 350, 500);
    
    //loading spinner view
    self.spinner = [[UIActivityIndicatorView alloc] init];
    self.spinner.color = [UIColor blueColor];
    self.spinner.backgroundColor = [UIColor whiteColor];
    self.spinner.frame = CGRectMake(0, 120, 350, 500);
    
    [self.tableView registerClass:UITableViewCell.class forCellReuseIdentifier:self.cellIdentifier];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) buttonPressed:(UIButton *)paramSender{
    [self.spinner startAnimating];
    [self.view addSubview:self.spinner];
    [self fetch:self.searchText.text];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.imageResults.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
    id item = [self.imageResults objectAtIndex:indexPath.row];
    cell.textLabel.text = [item objectForKey:@"content"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DetailViewController *vc = [[DetailViewController alloc] init];
    
    vc.imageDetail = [self.imageResults objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
