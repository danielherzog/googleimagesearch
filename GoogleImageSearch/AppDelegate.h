//
//  AppDelegate.h
//  GoogleImageSearch
//
//  Created by Daniel Herzog on 04.01.16.
//  Copyright (c) 2016 Daniel Herzog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

