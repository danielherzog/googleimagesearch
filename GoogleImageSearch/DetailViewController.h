//
//  ViewController.h
//  GoogleImageSearch
//
//  Created by Daniel Herzog on 04.01.16.
//  Copyright (c) 2016 Daniel Herzog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) NSDictionary *imageDetail;

@end

