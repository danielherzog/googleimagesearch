//
//  ViewController.m
//  GoogleImageSearch
//
//  Created by Daniel Herzog on 04.01.16.
//  Copyright (c) 2016 Daniel Herzog. All rights reserved.
//

#import "DetailViewController.h"
#import "GBLoading.h"

@interface DetailViewController ()

@property (strong, nonatomic) UIImageView *imageView;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = [self.imageDetail objectForKey:@"content"];
    self.imageView = [[UIImageView alloc] init];
    [self.view addSubview:self.imageView];
    
    
    [[GBLoading sharedLoading] loadResource:[self.imageDetail objectForKey:@"url"] withBackgroundProcessor:^id(NSData *rawData) {
        return [UIImage imageWithData:rawData];
    } success:^(id object) {
        UIImage *loadedImage = (UIImage *)object;
        [self.imageView setImage:loadedImage];
        self.imageView.frame = self.view.bounds;
        [self.imageView setNeedsDisplay];    } failure:^(BOOL isCancelled) {
        NSLog(@"failed to load");
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
